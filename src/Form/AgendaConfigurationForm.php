<?php

namespace Drupal\bordeaux_agenda_musee\Form;

use Drupal;
use Drupal\bordeaux_agenda_musee\TraitCommonForm;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Url;

/**
 * Dashboard des elements configurés : /admin/reports/bordeaux-agenda
 *
 * Class AgendaElementForm
 * @package Drupal\bordeaux_agenda_musee\Form
 */
class AgendaConfigurationForm extends ConfigFormBase {
  
  use TraitCommonForm;

  /**
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($config_factory);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'), $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bordeaux_agenda_musee.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return '';
  }


  public function getElementsAvailable() {
    if($this->isEntityNode()) {
      return Drupal::entityTypeManager()
        ->getStorage('node_type')
        ->loadMultiple();
    }
    else
    {
      $entity_type_manager = Drupal::service('entity_type.manager');
      return $entity_type_manager->getDefinitions();
    }
  }

  public function getLabel($entity) {
    if($this->isEntityNode()) {
      return $entity->label();
    }
    else
    {
      return $entity->getLabel();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity=null) {
    $this->setConfiguration($entity);
    $selectElementAvailable = [];
    $selectElementsUsed = [];
    $listElementsUsed = [];
    $selectElementAvailableLabel = $this->getElementsAvailable();
    $agendaElementsUsed = $this->getConfiguration()->get();
    foreach ($selectElementAvailableLabel as $elementType) {
      if(!array_key_exists($elementType->id(), $agendaElementsUsed)) {
        $selectElementAvailable[$elementType->id()] = $this->getLabel($elementType);
      }
      else {
        $selectElementsUsed[$elementType->id()] = $this->getLabel($elementType);
        $listElementsUsed[$elementType->id()] = $agendaElementsUsed[$elementType->id()];
      }
    }

    natsort($selectElementAvailable);
    asort($selectElementsUsed);
    asort($listElementsUsed);

    $form['fieldset_ajout'] = [
      '#type' => 'fieldset',
      '#title' => t('Configurer un élément @type', ['@type' => $this->getEntityType()]),
    ];

    $form['fieldset_ajout']['element'] = [
      '#type' => 'select',
      '#title' => t('Elément'),
      '#options' => $selectElementAvailable,
    ];

    $form['fieldset_ajout']['duplicate'] = [
      '#type' => 'checkbox',
      '#title' => t('Copier depuis un élément existant'),
      '#default_value' => FALSE,
    ];

    $form['fieldset_ajout']['agenda'] = [
      '#type' => 'select',
      '#options' => $selectElementsUsed,
      '#states' => [
        'visible' => [':input[name="duplicate"]' => ['checked' => TRUE],],
      ],
    ];

    $form['fieldset_ajout']['actions']['#type'] = 'actions';

    $form['fieldset_ajout']['actions']['ajouter'] = [
      '#type' => 'submit',
      '#name' => 'ajouter',
      '#value' => t('Ajouter'),
    ];

    $form['fieldset_table'] = [
      '#type' => 'fieldset',
    ];

    $form['fieldset_table']['table_element'] = [
      '#type' => 'table',
      '#header' => [$this->getEntityType(), 'Statut', 'Debug', 'Test', 'Actions'],
      '#tableselect' => FALSE,
    ];

    foreach ($listElementsUsed as $key => $element) {
      $form['fieldset_table']['table_element'][$key]['type'] = [
        '#type' => 'item',
        '#markup' => $selectElementsUsed[$key],
      ];

      $form['fieldset_table']['table_element'][$key]['Statut'] = [
        '#type' => 'item',
        '#markup' => $element['configuration']['active'] ? 'Oui' : 'Non',
      ];

      $form['fieldset_table']['table_element'][$key]['Debug'] = [
        '#type' => 'item',
        '#markup' => $element['configuration']['debug'] ? 'Oui' : 'Non',
      ];

      $form['fieldset_table']['table_element'][$key]['Test'] = [
        '#type' => 'item',
        '#markup' => $element['configuration']['test'] ? 'Oui' : 'Non',
      ];

      $route_parameters = ['entity_id' => $this->isEntityNode() ? 'node' : $key, 'bundle_id' => $key];
      $form['fieldset_table']['table_element'][$key]['actions'] = [
        '#type' => 'dropbutton',
        '#links' => [
          'configure' => [
            'title' => t('Configurer'),
            'url' => Url::fromRoute($this->getRouteEdit(), $route_parameters),
          ],
          'delete' => [
            'title' => t('Supprimer'),
            'url' => Url::fromRoute($this->getRouteDelete(), $route_parameters),
          ],
        ],
      ];
    }

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $trigger = $form_state->getTriggeringElement();
    $element = $form_state->getValue('element');
    if ($trigger['#name'] == 'ajouter' && $element) {
      $agenda = $form_state->getValue('agenda');
      if ($form_state->getValue('duplicate') && $agenda) {
        $this->dupplicate($agenda, $element);
      }
      $form_state->setRedirectUrl(Url::fromRoute($this->getRouteEdit(), ['entity_id' => $this->isEntityNode() ? 'node':  $element, 'bundle_id' => $element]));
    }
  }

  /**
   * Méthode permettant de récupérer la configuration d'un element pour le nouveau element à configurer
   * @param $key1
   * @param $key2
   */
  public function dupplicate($key1, $key2) {
    $config = Drupal::config($this->configurationName);
    $contenu = $config->get($key1);

    $config = Drupal::service('config.factory')->getEditable($this->configurationName);
    $config->set($key2, $contenu)->save();
  }
}
