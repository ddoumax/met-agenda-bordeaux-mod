<?php

namespace Drupal\bordeaux_agenda_musee\Controller;

use Drupal;
use Drupal\bordeaux_agenda_musee\Storage\BordeauxAgendaEntityStorage;
use Drupal\bordeaux_agenda_musee\Storage\BordeauxAgendaStorage;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Méthode permettant l'affichage des logs : /admin/reports/bordeaux-agenda-musee
 *
 * Class BordeauxAgendaController
 * @package Drupal\bordeaux_agenda_musee\Controller
 */
class BordeauxAgendaController extends ControllerBase {

  /**
   * @param $typeFile
   * @return string
   */
  public function getNameFile($typeFile) {
    $config = Drupal::config('bordeaux_agenda_musee.settings');
    $directory = $config->get('bordeaux_agenda_musee_debug_directory');
    return $directory . $typeFile.'-agenda.log';
  }

  /**
   * @return string
   */
  public function getNameTestFile() {
    return $this->getNameFile('test');
  }

  /**
   * @return string
   */
  public function getNameDebugFile() {
    return $this->getNameFile('agenda');
  }

  /**
   * @return array
   */
  public function logs() {

    if (isset($_GET['clearlogs']) && $_GET['clearlogs']) {
      if (file_exists($this->getNameTestFile())) {
        $file = fopen($this->getNameDebugFile(), 'w');
        fclose($file);
      }
      if (file_exists($this->getNameTestFile())) {
        $file = fopen($this->getNameDebugFile(), 'w');
        fclose($file);
      }
    }

    $header = array(
      // Icon column.
      '',
      array(
        'data' => $this->t('Events'),
        'class' => array(RESPONSIVE_PRIORITY_MEDIUM)
      ),
      array(
        'data' => $this->t('Post Date'),
        'field' => 'date_post',
        'sort' => 'desc',
        'class' => array(RESPONSIVE_PRIORITY_MEDIUM)
      ),
      array(
        'data' => $this->t('Post ID'),
        'field' => 'post_id',
        'class' => array(RESPONSIVE_PRIORITY_LOW)
      ),
    );

    $results = BordeauxAgendaStorage::getAll();
    $rows = array();
    foreach ($results as $result) {
      $entity = Drupal::entityTypeManager()
        ->getStorage($result->entity_type == 'node' ? 'node' : $result->entity_type)->load($result->entity_id);
      if ($entity != NULL) {
        $entityLink = Link::createFromRoute($entity->toUrl()->toString(),
          'entity.'.$result->entity_type.'.canonical',
          [$result->entity_type == 'node' ? 'node' : $result->entity_type => $result->entity_id]);
        $link = $entityLink->toString();
      } else {
        // L'entité n'existe plus.
        $link = t('@entity_type @entity_id deleted',
          ['@entity_type' => $result->entity_type, '@entity_id' => $result->entity_id]);
      }
      $rows[] = array(
        'data' => array(
          array('class' => array('icon')),
          array('data' => array('#markup' => $link)),
          date('d/m/Y - H:i:s', $result->post_date),
          array('data' => $result->post_id),
        ),
      );
    }

    $build['bordeaux_agenda_musee_log_table'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => array(
        'id' => 'bordeaux-agenda-logs',
        'class' => array('admin-dblog')
      ),
      '#empty' => $this->t('No log messages available.'),
      '#attached' => array(
        'library' => array('dblog/drupal.dblog'),
      ),
    );

    //link delete logs
    $clear_url = Url::fromUri('internal:/admin/reports/bordeaux-agenda-musee');
    $url_options = array(
      'query' => array('clearlogs' => TRUE),
      'attributes' => array('class' => 'button button-action button--primary button--small')
    );
    $clear_url->setOptions($url_options);
    $clear_link = Link::fromTextAndUrl(t('Clear logs'), $clear_url);
    $build['bordeaux_agenda_musee_delete_logs'] = array(
      '#markup' => $clear_link->toString(),
    );

    //get log file
    if (file_exists($this->getNameTestFile())) {
      $content = file_get_contents($this->getNameTestFile());
      if ($content) {
        $build['bordeaux_agenda_musee_log_test_file'] = array(
          '#markup' => '<h3>TEST logs file</h3><pre>' . $content . '</pre>',
        );
      }
    }

    if (file_exists($this->getNameDebugFile())) {
      $content = file_get_contents($this->getNameDebugFile());
      if ($content) {
        $build['bordeaux_agenda_musee_log_debug_file'] = array(
          '#markup' => '<h3>DEBUG logs file</h3><pre>' . $content . '</pre>',
        );
      }
    }
    return $build;
  }
}
