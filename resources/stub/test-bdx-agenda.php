<?php

/**
 * @file
 * Simulateur de la servlet Bdx Agenda /ebxCmPortlet/controlFormEvtServlet : récupère les valeurs postées,
 * les écrit dans un fichier 'NUM_SEQUENCE.txt', où 'NUM_SEQUENCE' est un incrément stocké dans le fichier 'sequence.txt'.
 * L'image transmise est déposée dans un repétoire 'upload'.
 *
 * Se dépose par exemple dans web/sandbox.
 * L'URL à configurer sera http://UN_SITE/sandbox/test-bdx-agenda.php (via /admin/config/services/bordeaux-agenda/settings).
 *
 * @return header Status code 200 concaténé au NUM_SEQUENCE.
 */

/* Contrôle des champs obligatoires. */

function control($champ) {
  if (empty($_POST[$champ])) {
    return 'Le champ ' . $champ .  ' est obligatoire. ';
  }
  return '';
}

$erreur = control('titre');
$erreur .= control('typeEvenement');
$erreur .= control('date');
$erreur .= control('resume');
$erreur .= control('entreeLibre');
$erreur .= control('vNom');
$erreur .= control('vCourriel');
$erreur .= control('isOkVerif');

if (!empty($erreur)) {
  header("HTTP/1.1 400 ERROR: " . $erreur);
  exit;
}

/* Récupération d'un numéro de séquence. */

$sequenceFile = fopen("sequence.txt", "r");
$sequence = fread($sequenceFile, filesize("sequence.txt"));
fclose($sequenceFile);

/* Récupération des infos sur la pièce jointe. */

$target_file = "";
$legendePhotoJointe = "";
$copyrightPhotoJointe = "";
if (isset($_FILES["photojointe"]["name"])) {
  $target_dir = "uploads/";
  $target_file = $target_dir . $sequence . "-" . basename($_FILES["photojointe"]["name"]);
  move_uploaded_file($_FILES["photojointe"]["tmp_name"], $target_file);
}
if (isset($_POST["legendePhotoJointe"])) {
  $legendePhotoJointe = $_POST["legendePhotoJointe"];
}
if (isset($_POST["copyrightPhotoJointe"])) {
  $copyrightPhotoJointe = $_POST["copyrightPhotoJointe"];
}

/* Récupération d'autres éléments transmis ; et écriture dans un fichier. */

$dataFile = fopen($sequence.".txt", "w");
fwrite($dataFile, "titre : " . ($_POST["titre"] ?? "") . "\n");
fwrite($dataFile, "typeEvenement : " . ($_POST["typeEvenement"] ?? "") . "\n");
fwrite($dataFile, "typePublic : " . ($_POST["typePublic"] ?? "") . "\n");
fwrite($dataFile, "date : '" . ($_POST["date"] ?? "") . "'\n");
fwrite($dataFile, "dateDebut : " . ($_POST["dateDebut"] ?? "") . "\n");
fwrite($dataFile, "dateFin : " . ($_POST["dateFin"] ?? "") . "\n");
fwrite($dataFile, "commentDate : " . ($_POST["commentDate"] ?? "") . "\n");
fwrite($dataFile, "resume : " . ($_POST["resume"] ?? "") . "\n");
fwrite($dataFile, "photojointe : " . $target_file . "\n");
fwrite($dataFile, "legendePhotoJointe : " . $legendePhotoJointe . "\n");
fwrite($dataFile, "copyrightPhotoJointe : " . $copyrightPhotoJointe . "\n");
fwrite($dataFile, "idLieu : " . ($_POST["idLieu"] ?? "") . "\n");
fwrite($dataFile, "commentaireLieu : " . ($_POST["commentaireLieu"] ?? "") . "\n");
fwrite($dataFile, "handi (2-Non, 1-Oui) : " . ($_POST["handi"] ?? "") . "\n");
fwrite($dataFile, "idOrganisateur : " . ($_POST["idOrganisateur"] ?? "") . "\n");
fwrite($dataFile, "renseignementOrganis : " . ($_POST["renseignementOrganis"] ?? "") . "\n");
fwrite($dataFile, "descDetail : " . ($_POST["descDetail"] ?? "") . "\n");
fwrite($dataFile, "entreeLibre (2-Non, 1-Oui) : " . ($_POST["entreeLibre"] ?? "") . "\n");
fwrite($dataFile, "tarifs : " . ($_POST["tarifs"] ?? "") . "\n");
// Possibilité de PJ, 'piecejointe1', 'piecejointe2', 'piecejointe2', 'piecejointe3' (non pris en charge par le module)
fwrite($dataFile, "vNom : " . ($_POST["vNom"] ?? "") . "\n");
// Possibilité 'vTel' (non pris en charge par le module)
fwrite($dataFile, "vCourriel : " . ($_POST["vCourriel"] ?? "") . "\n");
fwrite($dataFile, "liensEnSavoirPlus : " . ($_POST["liensEnSavoirPlus"] ?? "") . "\n");
fwrite($dataFile, "liensBilleterie : " . ($_POST["liensBilleterie"] ?? "") . "\n");
fwrite($dataFile, "isOkVerif (OK) : " . ($_POST["isOkVerif"] ?? "") . "\n");
fclose($dataFile);

/* Incrémentation d'un numéro de séquence. */

$sequenceFile = fopen("sequence.txt", "w");
fwrite($sequenceFile, ($sequence + 1));
fclose($sequenceFile);

header("HTTP/1.1 200 " . $sequence);
